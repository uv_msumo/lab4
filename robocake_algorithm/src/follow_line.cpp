#include "algorithm.h"

const double TURN_OVER = M_PI * 2;
const double TURN_LEFT = TURN_OVER / 4;
const double TURN_RIGHT = -TURN_LEFT;

const double DEFAULT_LINEAR = 0.15;

#define is_black(color) (color < threshold)


geometry_msgs::Twist control(
        std::unordered_map<std::string, double> proximity,
        std::unordered_map<std::string, double> reflectance,
        double threshold,
        geometry_msgs::Twist prev_msg
) {
    geometry_msgs::Twist msg;
    double left_front = reflectance["left_front"],
            right_front = reflectance["right_front"],
            left_back = reflectance["left_back"],
            right_back = reflectance["right_back"];

    std::cerr << "left_front = " << left_front
              << ", left_back = " << left_back
              << ", right_front = " << left_front
              << ", right_back = " << right_back
              << ", threshold = " << threshold << '\n';
    std::vector<bool> sensor{is_black(left_front), is_black(left_back), is_black(right_front), is_black(right_back)};

    // all on white
    if (sensor == std::vector < bool > {0, 0, 0, 0}) {
        std::cerr << "all white\n";
        std::cerr << "prev msg " << prev_msg << '\n';
        return prev_msg;
//        msg.linear.x = DEFAULT_LINEAR;
    }
    // one sensor on white
    if (sensor == std::vector < bool > {0, 0, 0, 1}) {
        std::cerr << "only right_back on black\n";
//        msg.linear.x = DEFAULT_LINEAR / 2;
    }
    if (sensor == std::vector < bool > {0, 0, 1, 0}) {
        std::cerr << "only right_front on black\n";
    }
    if (sensor == std::vector < bool > {0, 1, 0, 0}) {
        std::cerr << "only left_back on black\n";
        msg.angular.z = TURN_LEFT / 2;
        msg.linear.x = DEFAULT_LINEAR / 8;
    }
    if (sensor == std::vector < bool > {1, 0, 0, 0}) {
        std::cerr << "only left_front on back\n";
        msg.angular.z = TURN_LEFT / 4;
        msg.linear.x = DEFAULT_LINEAR / 8;
    }
    // two sensors on black
    if (sensor == std::vector < bool > {0, 0, 1, 1}) {
        std::cerr << "right on black left on white\n";
        msg.angular.z = TURN_OVER;
    }
    if (sensor == std::vector < bool > {0, 1, 0, 1}) {
        std::cerr << "back on black front on white\n";
        msg.angular.z = TURN_LEFT;
//        msg.linear.x = DEFAULT_LINEAR / 2;
    }
    if (sensor == std::vector < bool > {1, 0, 0, 1}) {
        std::cerr << "left_front and right_back on black\n";
    }
    if (sensor == std::vector < bool > {0, 1, 1, 0}) {
        std::cerr << "left_back and right_front on black\n";
    }
    if (sensor == std::vector < bool > {1, 0, 1, 0}) {
        std::cerr << "front on black, back on black\n";
        msg.angular.z = TURN_RIGHT;
//        msg.linear.x = DEFAULT_LINEAR / 2;
    }
    if (sensor == std::vector < bool > {1, 1, 0, 0}) {
        std::cerr << "left on black, right on white\n";
        msg.linear.x = DEFAULT_LINEAR;
    }
    // three sensors on black
    if (sensor == std::vector < bool > {0, 1, 1, 1}) {
        std::cerr << "only left_front on white\n";
    }
    if (sensor == std::vector < bool > {1, 0, 1, 1}) {
        std::cerr << "only left_back on white\n";
    }
    if (sensor == std::vector < bool > {1, 1, 0, 1}) {
        std::cerr << "only right_front on white\n";
        msg.angular.z = TURN_LEFT;
    }
    if (sensor == std::vector < bool > {1, 1, 1, 0}) {
        std::cerr << "only right_back on white\n";
        msg.angular.z = TURN_RIGHT / 2;
        msg.linear.x = DEFAULT_LINEAR / 4;
    }
    // four sensors on black
    if (sensor == std::vector < bool > {1, 1, 1, 1}) {
        std::cerr << "all black\n";
        msg.linear.x = -DEFAULT_LINEAR;
    }
    return msg;
} 
